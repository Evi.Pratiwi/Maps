package id.ac.unpas.chatapp;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;

public class MainActivity extends AppCompatActivity {

    private WebSocketClient mWebSocketClient;
    private EditText mEditTextChats;
    private EditText mEditTextMessage;
    private Button mButtonSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mEditTextChats = findViewById(R.id.editTextChats);
        mEditTextMessage = findViewById(R.id.editTextMessage);
        mButtonSend= findViewById(R.id.buttonSend);

        connectWebSocker();

        mButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptSend();
            }
        });
    }

    private void connectWebSocker() {
        URI uri;
        try{
            //setting ip yang dituju, jembatannya
            //ip situ unpas, yang dimodul ip hostpot
            uri = new URI("ws://172.22.62.4:8887");
        }catch (URISyntaxException e){
            e.printStackTrace();
            return;
        }

        mWebSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                Log.i("Websocket", "Opened");
                mWebSocketClient.send("Hello From" + Build.MANUFACTURER+ " "+Build.MODEL);
            }

            @Override
            public void onMessage(String s) {
                final String message = s;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mEditTextChats.append(message+"\n");
                    }
                });
            }

            @Override
            public void onClose(int i, String s, boolean b) {
                Log.i("Websocket", "Closed "+s);

            }

            @Override
            public void onError(Exception ex) {
                Log.i("Websocket", "Error "+ ex.getMessage());
            }
        };
        mWebSocketClient.connect();
    }

    private void attemptSend(){
        String message = mEditTextMessage.getText().toString().trim();
        if(TextUtils.isEmpty(message)){
            return;
        }
        mEditTextMessage.setText("");
        mWebSocketClient.send(message);
    }
}
